from django.apps import AppConfig


class AudioTestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'audio_test'
